# ng2-study
<blockquote> It's a blog project for ng2</blockquote>
<code>ng2 version => 4.4.6</code>

<h2>Build Setup</h2>

<pre>
# install dependencies
npm install

cd mock 

npm i -g nei

nei server

open a new cmd window

continue below step

# serve with hot reload at localhost:*
ng serve

# build for production with minification
ng build
</pre>

<h2>一个ng2+的学习程序，主要把常用到的点摸一遍</h2>

<pre>
  ts开发
  nei进行mock数据及API管理
  组件
    App
    |-Header
    |-router-outlet
      主页(page/Index/Home)
          |-热门关键词(HotTag)
          |-置顶推荐(TopList)
          |-最新评论(NewRemark)
          |-友情链接(FriendLink)
          |-文章搜索(AllSearch)
      列表(page/Index/List)
      详情(page/Index/Detail)
          |-文章详情(ArticleDetail)
          |-评论框(CommentEditor)
          |-热门关键词(HotTag)
          |-置顶推荐(TopList)
          |-最新评论(NewRemark)
          |-文章搜索(AllSearch)
    |-Footer
    
    服务(service)
    |-Api
    |-bus(发布订阅)
    |-config(数据持久化)
    |-routerParams(路由参数获取)
    |-utils(常用工具方法)
    
    过滤器(pipe)
    |-limitToPipe(限制字符串长度)
    
    sass(对cube进行精简扩展的一套微型sass库)
    |-style.scss
      |-cube.scss
        @import '_neat.scss';(样式重置)
        @import'_layout_pc.scss';(适用pc的布局，兼容性好)
        @import'_layout_m.scss';(适用移动端的布局，写法更精简)
          example(以下全部作用于容器上，简单举几个例子)
            |-class="df"(元素横着排)
            |-class='df jcc'(元素横着居中)
            |-class='df jcsb'(元素横着两端对齐)
            |-class='df fdr aic'(未知元素垂直居中)
          注释：
          上方是不是看起来有点麻烦？没关系，我来解释下
          df => display:flex
          jcc => justify-content:center
          jcsb => justify-content:space-between
          dfr => flex-direction:row
          aic => align-items:center
          (豁然开朗有木有☺)
        @import '_utils.scss';(常用工具)
        @import '_button.scss';(按钮组)
        @import '_print.scss';（打印样式）
        @import '_form.scss';(表单样式，借鉴amziui)
        @import '_lib.scss';(封装常用函数)
        @import '_fittext.scss';(自适应文本)
    
</pre>
