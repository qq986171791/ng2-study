/*
 * config file for nei server command
 * @author 
 * Auto build by NEI Builder
 */
var path = require('path');
module.exports = {
    /* 根目录 */
    webRoot: '',
    /* 视图目录 */
    viewRoot: '',
    /* 路由 */
    routes: {
      //"ALL /api/*": "代理所有接口, 这里输入代理服务器地址",
      "GET /api/tops/": { path: 'get/api/tops/data', id: 50000, group: 'top' },
            "DELETE /api/tags/:id": { path: 'delete/api/tags/_/id/data', id: 49962, group: 'tag' },
            "PATCH /api/tags/:id": { path: 'patch/api/tags/_/id/data', id: 49961, group: 'tag' },
            "GET /api/tags/:id": { path: 'get/api/tags/_/id/data', id: 49958, group: 'tag' },
            "POST /api/tags/": { path: 'post/api/tags/data', id: 49960, group: 'tag' },
            "GET /api/tags/": { path: 'get/api/tags/data', id: 49959, group: 'tag' },
            "DELETE /api/tags/": { path: 'delete/api/tags/data', id: 49963, group: 'tag' },
            "GET /api/links/:id": { path: 'get/api/links/_/id/data', id: 50069, group: 'link' },
            "PATCH /api/links/:id": { path: 'patch/api/links/_/id/data', id: 50072, group: 'link' },
            "DELETE /api/links/:id": { path: 'delete/api/links/_/id/data', id: 50073, group: 'link' },
            "DELETE /api/links/": { path: 'delete/api/links/data', id: 50074, group: 'link' },
            "GET /api/links/": { path: 'get/api/links/data', id: 50070, group: 'link' },
            "POST /api/links/": { path: 'post/api/links/data', id: 50071, group: 'link' },
            "DELETE /api/comments/:id": { path: 'delete/api/comments/_/id/data', id: 50008, group: 'comment' },
            "PATCH /api/comments/:id": { path: 'patch/api/comments/_/id/data', id: 50007, group: 'comment' },
            "GET /api/comments/:id": { path: 'get/api/comments/_/id/data', id: 50004, group: 'comment' },
            "POST /api/comments/": { path: 'post/api/comments/data', id: 50006, group: 'comment' },
            "GET /api/comments/": { path: 'get/api/comments/data', id: 50005, group: 'comment' },
            "DELETE /api/comments/": { path: 'delete/api/comments/data', id: 50009, group: 'comment' },
            "GET /api/articles/:id": { path: 'get/api/articles/_/id/data', id: 49694, group: 'article' },
            "PATCH /api/articles/:id": { path: 'patch/api/articles/_/id/data', id: 49697, group: 'article' },
            "DELETE /api/articles/:id": { path: 'delete/api/articles/_/id/data', id: 49698, group: 'article' },
            "DELETE /api/articles/": { path: 'delete/api/articles/data', id: 49699, group: 'article' },
            "POST /api/articles/": { path: 'post/api/articles/data', id: 49696, group: 'article' },
            "GET /api/articles/": { path: 'get/api/articles/data', id: 49695, group: 'article' },
          },
    /* 注入给页面的模型数据的服务器配置 */
    // modelServer: {
    //     // 完整的主机地址，包括协议、主机名、端口
    //     host: '',
    //     // 查询参数，键值对
    //     queries: {},
    //     // 自定义请求头，键值对
    //     headers: {},
    //     // path 可以是字符串，也可以是函数；默认不用传，即使用 host + 页面path + queries 的值
    //     // 如果是函数，则使用函数的返回值，传给函数的参数 options 是一个对象，它包含 host、path（页面path）、queries、headers 等参数
    //     // 如果 path 的值为假值，则使用 host + 页面path + queries 的值；
    //     // 如果 path 的值是相对地址，则会在前面加上 host
    //     // path: '',
    // },
    /* api 响应头 */
    apiResHeaders: {
        'Access-Control-Allow-Origin': 'http://localhost:4200',
        'Access-Control-Allow-Credentials': 'true',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Methods': 'GET, POST, HEAD, OPTIONS, PUT, DELETE, PATCH',
    },
    /* 是否自动打开浏览器 */
    launch: true,
    /* 自动打开的页面地址 */
    openUrl: '',
    /* 端口 */
    port: 8002,
    /* 是否使用 https 协议，设为true的时候表示启用 */
    https: false,
    /* 是否使用 nei 提供的在线 mock 数据 */
    online: false,
    /* 是否监听静态文件和模板文件的变化并自动刷新浏览器 */
    reload: true,
    /* 文件监听的选项 */
    watchingFiles: {
        /* 需要即时编译的文件, 前提是 reload 为 true */
        compilers: {
            /* 值为 mcss 的配置选项, 默认为 false，即不编译 mcss 文件 */
            mcss: false
        },
        /* 不用监听的文件，支持通配符 */
        //ignored: '**/*.css'
    },
    /* 项目的 key */
    projectKey: 'e131aec0a86bfd93dc3231a337881d2d',
    /* 同步模块mock数据路径 */
    mockTpl: 'D:/workspace/angular/mock/mock.data/template/',
    /* 异步接口mock数据路径 */
    mockApi: 'D:/workspace/angular/mock/mock.data/interface/',
    /* 模板后缀 */
    viewExt: '.html',
    /* 模板引擎 */
    engine: 'smarty',
    /* 打开下面的 fmpp 配置，可以在模板中调用自定义 jar 包中的类 */
    //fmpp: {
    //    /* 存放自定义 jar 的目录, 绝对路径 */
    //    jarDir: '',
    //    /* 暴露给模板的类实例名称和 jar 中的类名(带包名)的对应关系 */
    //    jarConfig: {
    //        [暴露给模板的类实例名称]: [类名] // 比如: HighlightUtil: 'yueduutil.HighlightUtil'
    //    }
    //}
};