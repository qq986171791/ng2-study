import { Component, Input, OnChanges } from '@angular/core';
import { ApiService } from "../../../service/ApiService"
@Component({
  selector: '[CommonHotTag]',
  templateUrl: 'CommonHotTag.component.html',
  styleUrls: ['CommonHotTag.component.scss']
})
export class CommonHotTagComponent implements OnChanges{
  public title: string = 'CommonHotTag';
  public tagList: any = [];
  @Input() margin: any = {
    left:0,
    top:0,
    right:0,
    bottom:0
  };
  constructor(private apiService: ApiService){
      this.apiService.getTagList().subscribe((res: Response) => {
        if(0 == res['code']){
          this.tagList = res['result'];
        }
      })
  }
  ngOnChanges(): void{

  }

}
