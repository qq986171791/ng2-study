import { Component, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { ApiService } from "../../../service/ApiService"
@Component({
  selector: '[CommonEditor]',
  templateUrl: 'CommonEditor.component.html',
  styleUrls: ['CommonEditor.component.scss']
})
export class CommonEditorComponent implements OnChanges{
  public title: string = '评论框';
  @Input() margin: any = {
    left:0,
    top:0,
    right:0,
    bottom:0
  };
  @Output() comment: EventEmitter<any> = new EventEmitter();
  public data: any = {
    content:"",
    email:""
  };
  constructor(private apiService: ApiService){
    this._init();
  }

  private _init(): void{

  }
  ngOnChanges(): void{

  }
  //回复
  public doComment(): void{
    this.comment.emit(this.data);
  }
}

