import {Component, Input, OnChanges, Directive} from '@angular/core';
import { ApiService } from "../../../service/ApiService"
import { RouterParamsService } from '../../../service/RouterParamsService'
import {stringifyElement} from "@angular/platform-browser/testing/src/browser_util";

@Component({
  selector: '[CommonCommentEditor]',
  templateUrl: 'CommonCommentEditor.component.html',
  styleUrls: ['CommonCommentEditor.component.scss']
})
export class CommonCommentEditorComponent implements OnChanges{
  public title: string = '评论框';
  public tagList: any = [];
  public commentList: any = [];
  @Input() margin: any = {
    left:0,
    top:0,
    right:0,
    bottom:0
  };
  @Input() article: any = {};
  constructor(
    private apiService: ApiService,
    private routerParamsService: RouterParamsService
  ){
    this._init();
    this._on();
  }

  private _init(): void{
    this._initCommentList();
  }
  private _on(): void{

  }
  public onComment(e): void{
      console.log(this.article);
    let params: Object = {
      aid:this.article.aid,
      content:e.content,
      email:e.email,
      ouid:"by-login-user-provide",
      pid:'comment-to-who?'
    };
    this._postComment(params);
  }
  //新建评论
  private _postComment(params: Object): void{
    this.apiService.postComment(params).subscribe((res: Response) => {

    });
  }
  //初始化评论列表
  private _initCommentList(options?: any): void{
    this.apiService.getCommentList(options).subscribe((res: Response) => {
      if(0 == res['code']){
        this.commentList = res['result'];
      }
    })
  }
  ngOnChanges(): void{

  }
  //回复
  public doComment(type: string): void{
    let strategy:any = {article: null,user: null};
    //回复给文章
    strategy.article = () => {

    };
    //回复给用户
    strategy.user = () => {

    };
    strategy[type]();
  }

}
