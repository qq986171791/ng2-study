import { Component,OnInit,Input,OnChanges } from '@angular/core';
import { Router } from '@angular/router'
import { ApiService } from "../../../service/ApiService"
import { BusService } from '../../../service/BusService'
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import {HttpParams} from "@angular/common/http";
@Component({
  selector: '[CommonArticleDetail]',
  templateUrl: 'CommonArticleDetail.component.html',
  styleUrls: ['CommonArticleDetail.component.scss']
})
export class CommonArticleDetailComponent implements OnInit,OnChanges {
  public title: string = 'CommonArticleDetail';
  @Input() article: any = {};
  public preAid:string;
  public nextAid:string;
  constructor(
    private router: Router,
    private apiService: ApiService,
    private busService: BusService
  ){
    this.init();
    this.on();
  }

  ngOnInit(): void{

  }

  ngOnChanges():void{
    if(this.article && this.article.pre && this.article.next){
      this.preAid = this.article.pre.aid;
      this.nextAid = this.article.next.aid;
    }
  }

  private init(): void{

  }

  private on(): void{

  }


}
