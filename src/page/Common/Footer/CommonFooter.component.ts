import { Component } from '@angular/core';

@Component({
  selector: '[CommonFooter]',
  templateUrl: 'CommonFooter.component.html',
  styleUrls: ['CommonFooter.component.scss']
})
export class CommonFooterComponent {
  title = 'CommonFooter';
}
