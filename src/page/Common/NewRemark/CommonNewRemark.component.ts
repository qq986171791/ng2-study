import { Component,Input } from '@angular/core';
import { ApiService } from '../../../service/ApiService'
@Component({
  selector: '[CommonNewRemark]',
  templateUrl: 'CommonNewRemark.component.html',
  styleUrls: ['CommonNewRemark.component.scss'],
})
export class CommonNewRemarkComponent {
  public title: string = 'CommonNewRemark';
  public remarkList: any = [];
  @Input() margin: any = {
    top:0,
    right:0,
    bottom:0,
    left:0
  };
  constructor(
    private apiService: ApiService
  ){
    this.apiService.getNewRemark().subscribe((res:Response)=>{
      if(0 == res['code']){
        this.remarkList = res['result'];
      }
    });
  }
}
