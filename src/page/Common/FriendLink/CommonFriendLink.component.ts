import { Component,Input } from '@angular/core';
import { ApiService } from '../../../service/ApiService'
@Component({
  selector: '[CommonFriendLink]',
  templateUrl: 'CommonFriendLink.component.html',
  styleUrls: ['CommonFriendLink.component.scss']
})
export class CommonFriendLinkComponent {
  public title: string = 'CommonFriendLink';
  public friendLinkList = [];
  @Input() margin: any = {};
  constructor(
    private apiService: ApiService
  ){
    this.apiService.getFriendLinkList().subscribe((res: Response) => {
      if(0 == res['code']){
        this.friendLinkList = res['result'];
      }
    })
  }
}
