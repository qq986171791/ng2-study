import { Component, Input, OnChanges } from '@angular/core';
import { ApiService } from "../../../service/ApiService"
@Component({
  selector: '[CommonTopList]',
  templateUrl: 'CommonTopList.component.html',
  styleUrls: ['CommonTopList.component.scss']
})
export class CommonTopListComponent implements OnChanges{
  public title: string = 'CommonTopList';
  public topList: any = [];
  @Input() margin: any = {
    top:0,
    bottom:0,
    left:0,
    right:0
  };
  constructor(private apiService: ApiService){
    this.apiService.getTopList().subscribe((res: Response) => {
      if(0 == res['code']){
        this.topList = res['result'];
      }
    });
  }

  ngOnChanges():void{

  }


}
