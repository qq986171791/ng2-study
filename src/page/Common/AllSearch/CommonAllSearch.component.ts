import { Component,Input } from '@angular/core';
import { BusService } from '../../../service/BusService'

@Component({
  selector: '[CommonAllSearch]',
  templateUrl: 'CommonAllSearch.component.html',
  styleUrls: ['CommonAllSearch.component.scss']
})
export class CommonAllSearchComponent {
  public data: any = {
    title:""
  };
  @Input() margin: any = {
    top:0,
    right:0,
    bottom:0,
    left:0
  };
  private title: string = 'CommonAllSearch';
  constructor(private busService: BusService){
    this.init();
    this.on();
  }
  private init(): void{

  }
  private on(): void{

  }
  //触发搜索文章列表
  private triggerSearchArticleList(): void{
    this.busService.onSearchArticleList.emit({
      data:this.data
    });
  }

}
