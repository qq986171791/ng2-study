import { Component,OnInit,Input } from '@angular/core';
import { Router } from '@angular/router'
import { ApiService } from "../../../service/ApiService"
import { BusService } from '../../../service/BusService'
import {RequestOptions, Request, RequestMethod} from '@angular/http';
import {HttpParams} from "@angular/common/http";
@Component({
  selector: '[CommonArticleList]',
  templateUrl: 'CommonArticleList.component.html',
  styleUrls: ['CommonArticleList.component.scss']
})
export class CommonArticleListComponent implements OnInit {
  public title: string = 'CommonArticleList';
  public articleList: any = [];
  @Input() margin: any = {
    top:0,
    right:0,
    bottom:0,
    left:0
  };
  private onClickItem = null;
  constructor(
    private router: Router,
    private apiService: ApiService,
    private busService: BusService
  ){
    this.init();
    this.on();
  }

  ngOnInit(): void{

  }

  toDetail(onClickItem: any): void{
    let aid = onClickItem.aid;
    this.setOnClickItem(onClickItem);
    this.router.navigate(['/Index-Detail',aid]);
  }

  private setOnClickItem(onClickItem: any): void{
    this.onClickItem = onClickItem;
  }

  //初始化文章列表
  private initArticleList(options?: any): void{
    this.apiService.getArticleList(options).subscribe((res: Response) => {
      if(0 == res['code']){
        this.articleList = res['result'];
      }
    })
  }

  private init(): void{
      this.initArticleList();
  }

  private on(): void{
      this.onSearchArticleList();
  }

  private onSearchArticleList(): void{;
    let params = new HttpParams();
      this.busService.onSearchArticleList.subscribe((res: any)=>{
        let data = res.data;
        let params = new HttpParams()
          .set('title',data.title);
        this.initArticleList({
          params
        });
      });
  }

}
