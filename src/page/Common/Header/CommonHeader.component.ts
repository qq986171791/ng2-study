import { Component } from '@angular/core';

@Component({
  selector: '[CommonHeader]',
  templateUrl: 'CommonHeader.component.html',
  styleUrls: ['CommonHeader.component.scss']
})
export class CommonHeaderComponent {
  title = 'CommonHeader';
}
