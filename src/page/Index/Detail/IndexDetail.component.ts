import {Component, DoCheck, OnInit} from '@angular/core';
import { RouterParamsService } from '../../../service/RouterParamsService'
import { ApiService } from '../../../service/ApiService'
@Component({
  selector: 'IndexDetail',
  templateUrl: 'IndexDetail.component.html',
  styleUrls: ['IndexDetail.component.scss'],
  providers: [RouterParamsService]
})
export class IndexDetailComponent implements OnInit{
  public title: string = "我是標題";
  public article: any = {};
  private aid: string = null;
  constructor(
    private routerParamsService: RouterParamsService,
    private apiService: ApiService,
  ){
    this._init();
    this._on();
  }
  ngOnInit(): void{

  }
  private _init(): void{
      this._initArticle();
  }
  private _on(): void{

  }
  private _initArticle(): void{
    this.aid = this.routerParamsService.routerParams['aid'];
    this.apiService.getArticle(this.aid).subscribe((res: Response) => {
      if(0 == res['code']){
        this.article = res['result'];
      }
    });
  }
}
