import { Component } from '@angular/core';
import { ApiService } from '../../../service/ApiService'
import { ConfigService } from '../../../service/ConfigService'
@Component({
  selector: '[IndexHome]',
  templateUrl: 'IndexHome.component.html',
  styleUrls: ['IndexHome.component.css'],
  providers: [ApiService,ConfigService]
})
export class IndexHomeComponent {
  public title: string = 'IndexHome';
  constructor(private api: ApiService,private config: ConfigService) {

  }
  printf(){
    this.api.getArticleList();
  }
}
