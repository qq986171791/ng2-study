import { Component } from '@angular/core';

@Component({
  selector: 'IndexList',
  templateUrl: 'IndexList.component.html',
  styleUrls: ['IndexList.component.css']
})
export class IndexListComponent {
  title = 'IndexList';
}
