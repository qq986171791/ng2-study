import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { IndexHomeComponent } from '../page/Index/Home/IndexHome.component'
import { IndexListComponent } from '../page/Index/List/IndexList.component'
import { IndexDetailComponent } from '../page/Index/Detail/IndexDetail.component'

const routes: Routes = [
  { path: '', redirectTo: '/Index-Home', pathMatch: 'full' },
  { path: 'Index-Home',  component: IndexHomeComponent },
  { path: 'Index-List', component: IndexListComponent },
  { path: 'Index-Detail/:aid',     component: IndexDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
