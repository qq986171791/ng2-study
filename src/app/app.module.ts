import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule,HTTP_INTERCEPTORS} from '@angular/common/http';
import { ApiService } from '../service/ApiService'

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module'
import { FormsModule } from '@angular/forms'

import { IndexHomeComponent } from '../page/Index/Home/IndexHome.component'
import { IndexListComponent } from '../page/Index/List/IndexList.component'
import { IndexDetailComponent } from '../page/Index/Detail/IndexDetail.component'
import { CommonHeaderComponent } from '../page/Common/Header/CommonHeader.component'
import { CommonFooterComponent } from '../page/Common/Footer/CommonFooter.component'
import { CommonArticleListComponent } from '../page/Common/ArticleList/CommonArticleList.component'
import { CommonHotTagComponent } from '../page/Common/HotTag/CommonHotTag.component'
import { CommonTopListComponent } from '../page/Common/TopList/CommonTopList.component'
import { CommonNewRemarkComponent } from '../page/Common/NewRemark/CommonNewRemark.component'
import { CommonFriendLinkComponent } from '../page/Common/FriendLink/CommonFriendLink.component'
import { CommonAllSearchComponent } from '../page/Common/AllSearch/CommonAllSearch.component'
import { CommonArticleDetailComponent } from '../page/Common/ArticleDetail/CommonArticleDetail.component'
import { CommonCommentEditorComponent } from '../page/Common/CommentEditor/CommonCommentEditor.component'
import { CommonEditorComponent } from '../page/Common/Editor/CommonEditor.component'

import { RouterParamsService } from '../service/RouterParamsService'
import { MockService } from '../service/MockService'
import { ConfigService } from '../service/ConfigService'
import { NoopInterceptorService } from "../service/NoopInterceptorService";

import { LimitToPipe } from '../pipe/string/LimitToPipe'
import { BusService } from "../service/BusService";
import { UtilsService } from '../service/UtilsService'


@NgModule({
  declarations: [
    AppComponent,
    IndexHomeComponent,
    IndexListComponent,
    IndexDetailComponent,
    CommonHeaderComponent,
    CommonFooterComponent,
    CommonArticleListComponent,
    CommonHotTagComponent,
    CommonTopListComponent,
    CommonNewRemarkComponent,
    CommonFriendLinkComponent,
    CommonAllSearchComponent,
    CommonArticleDetailComponent,
    CommonCommentEditorComponent,
    CommonEditorComponent,
    LimitToPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    RouterParamsService,
    ConfigService,
    ApiService,
    MockService,
    { provide: HTTP_INTERCEPTORS, useClass: NoopInterceptorService, multi: true },
    BusService,
    UtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {  }


