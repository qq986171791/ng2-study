import { Injectable,EventEmitter } from '@angular/core'


@Injectable()
export class BusService{
  public onSearchArticleList: EventEmitter<any>;
  constructor(){
    this.onSearchArticleList = new EventEmitter();
  }
}
