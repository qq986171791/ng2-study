import { Injectable,AfterViewInit } from '@angular/core';
import { Router,Params,ActivatedRoute } from '@angular/router'

@Injectable()
export class RouterParamsService{
  public routerParams:Object = {};
  constructor(
    private router: Router,
    private activatedRouter:ActivatedRoute
  ){
    this.activatedRouter.params.subscribe(
      params => {
        for(let key in params){
          this.routerParams[key] = params[key];
        }
        console.log('////////////////////自定义调试信息开始');
        console.log('当前页面:',this.router.url,);
        console.log("路由参数",this.routerParams);
        console.log("路由对象",this);
        console.log('自定义调试信息结束////////////////////');
      }
    )
  }
}
