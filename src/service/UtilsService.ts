import { Injectable } from '@angular/core'


@Injectable()
export class UtilsService{
  constructor(){

  }
  /**
   * Utility function to check if an item is an array
   * @function #isArray
   * @namespace Utils
   * @param {Object} obj - The item to check
   * @returns {Boolean} - True if the argument is a array
   * @author   Toney,
   * @dateTime 2017-04-03,
   */
  public isArray(obj: any) {
    var str = Object.prototype.toString.call(obj);
    return str === '[object Array]' || str === '[object Array Iterator]';
  }
  /**
   * Utility function to check if an item is of type object
   * @function #isObject
   * @namespace Utils
   * @param {Object} obj - The item to check
   * @param {Boolean} [strict=false] - Also checks that the object is not an
   * array
   * @returns {Boolean} - True if the argument is an object.
   * @author   Toney,
   * @dateTime 2017-04-03,
   */
  public isObject(obj: any, strict?: boolean) {
    return obj && typeof obj === 'object' && (!strict || !this.isArray(obj));
  }
  // 拷浅拷贝
  public extend(obj: any,newObj?: any,dep?:boolean): any{
    if(!newObj){
      return obj;
    }
    let strategy = {};
    strategy['deep'] = () => {
      for(let key in newObj){
        if(this.isObject(newObj[key]) || this.isArray(newObj[key])){
          this.extend(obj[key],newObj[key]);
          continue;
        }
        obj[key] = newObj[key];
      }
    };
    strategy['normal'] = () => {
      for(let key in newObj){
        obj[key] = newObj[key];
      }
    }
    strategy[dep ? 'deep' : 'normal']();
    return obj;
  }
}
