import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest,HttpResponse,HttpEventType} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

/**什么也不做，只是简单的转发请求而不做任何修改*/
@Injectable()
export class NoopInterceptorService implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).map((event: any) => {
            if(HttpEventType.Response === event.type){
                console.log(111111111111);
            }
            return event;
        });
    }
}
