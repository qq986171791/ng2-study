import { Injectable,ReflectiveInjector } from '@angular/core'
import {HttpClient,HttpParams} from "@angular/common/http"
import {fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, ConnectionBackend, Http, RequestOptions} from '@angular/http';
import {Response, ResponseOptions} from '@angular/http';
import { MockService } from '../service/MockService'
import { ConfigService } from "./ConfigService"
import { UtilsService } from '../service/UtilsService'


@Injectable()
export class ApiService{
  private api: any = {
    article:"/api/articles/",
    tag:"/api/tags/",
    top:'/api/tops/',
    remark:'/api/comments/',
    friendLink:'/api/links/',
    comment:'/api/comments/',
  };
  private httpParams: HttpParams;
  constructor(
    private http: HttpClient,
    private mockService:MockService,
    private config: ConfigService,
    private utilsService: UtilsService
  ) {

    this.initApi();
  }
  initApi(): void{
    for(let key in this.api){
      this.api[key] = this.config.publicUrl + this.api[key];
    }
  }
  //获取文章列表
  getArticleList(options: Object = {}){
    return this.http.get(
      this.api.article,
      options
    );
  }
  //获取文章
  getArticle(cid){

    let httpParams = new HttpParams()
      .set('cid',cid);

    return this.http.get(
      this.api.article+":cid",
      {
        params: httpParams
      }
    );
  }
  //获取标签列表
  getTagList(){
    return this.http.get(
      this.api.tag
    );
  }

  //获取置顶推荐
  getTopList(){
    return this.http.get(
      this.api.top
    );
  }
  //获取最新评论
  getNewRemark(){
    return this.http.get(
      this.api.remark
    );
  }
  //获取外链
  getFriendLinkList(){
    return this.http.get(
      this.api.friendLink
    );
  }

  //获取评论列表
  getCommentList(options: Object = {}){
    return this.http.get(
      this.api.comment,
      options
    );
  }
  //新建评论
  postComment(params){
    let body = JSON.stringify({
      content:params.content,
      email : params.email,
      aid:params.aid,
      ouid:params.ouid,
      pid:params.pid
    });
    return this.http.post(
      this.api.comment,
      body
    );
  }
}
