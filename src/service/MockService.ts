import { Injectable,ReflectiveInjector } from '@angular/core'
import Mock from '../typeslib/mock/Mock'
import { ResponseOptions } from '@angular/http';
import { HttpRequest } from '@angular/common/http';

import { ConfigService } from "./ConfigService"
@Injectable()
export class MockService{
  private mock: any = Mock.mock;
  private random: any = Mock.Random;
  constructor(
      private config: ConfigService,
  ) {
      // this.mock(this.config.domain + '/index.php/restful/getList?id=1',{});
  }

  getArticleList():any[]{
    return this.mock([
      {
        title:"angular2使用sass,scss",
        author:"张庆东",
        createTime:new Date().getTime(),
        "category|1":["前端","大杂烩","js"],
        abs:Mock.mock('@paragraph'),
        listImg:this.random.image("220x150")
      }
    ]);
  }

}
